An LTS security update is needed for the following source packages.
When you add a new entry, please keep the list alphabetically sorted.

The specific CVE IDs do not need to be listed, they can be gathered in an up-to-date manner from
https://security-tracker.debian.org/tracker/source-package/SOURCEPACKAGE
when working on an update.

To pick an issue, simply add your name behind it. To learn more about how
this list is updated have a look at
https://wiki.debian.org/LTS/Development#Triage_new_security_issues

--
ansible (Chris Lamb)
--
cairo
  NOTE: 20181024: No fix available yet.
--
enigmail (Antoine Beaupre)
  NOTE: 20180926: see 871s9fps8e.fsf@curie.anarc.at before working on this (anarcat)
--
icecast2 (Abhijith PA)
  NOTE: 20181106: please upload https://git.fosscommunity.in/bhe/patches/raw/master/icecast2_deb8u2.debdiff
--
icu (Roberto C. Sánchez)
--
imagemagick (Thorsten Alteholz)
  NOTE: 20181023: add additional Ubuntu patch to disable ghostscript handled formats
  NOTE: 20181023: wait with upload until this is done in unstable -> #907336
  NOTE: 20181110: bug still open so upload without ubuntu patch
--
jasper (apo)
  NOTE: 20181104: consider fixing no-dsa issues too because the package is used
  NOTE: by almost 50 % of sponsors. (apo)
--
libapache-mod-jk
  NOTE: 20181104: I contacted the security team and asked about upgrading the
  NOTE: package to the latest upstream version because the changes are rather
  NOTE: intrusive. (apo)
--
libav (Hugo Lefeuvre)
  NOTE: 20180118: Diego Biurrun (from the libav team) was working on patches, but encountered personal issues and had to stop.
  NOTE: 20180118: It is unlikely that he will start again in the next weeks.
  NOTE: 20180118: I am currently working on CVE triage but I will not be able to process the whole backlog until May. (Hugo)
  NOTE: 20180529: Help is welcome, feel free to mail Hugo. Still up-to-date. Help needed for CVE triage and patch development.
  NOTE: 20180529: Just contacted some of the CVE reporters to ask for the reproducers, CC-ed team ML.
--
liblivemedia (Hugo Lefeuvre)
  NOTE: CVE entry says remote: "no", but it looks like a pretty exploitable remote vulnerability
  NOTE: (remote code execution)... CVE is very well documented so I think this is worth a patch
--
linux (Ben Hutchings)
--
linux-4.9 (Ben Hutchings)
--
mysql-connector-java
--
nsis (Thorsten Alteholz)
  NOTE: 20181007: Windows installer, but issue was reported by gpg4win so
  NOTE: 20181007: likely affects UNIX systems. (Chris Lamb)
  NOTE: 20181110: waiting for email answer
--
openjdk-7
--
openjpeg2 (Hugo Lefeuvre)
  NOTE: 20181022: wrote patches for CVE-2018-5785 and CVE-2017-17480, waiting for upstream
  NOTE: to approve CVE-2017-17480 before upload.
  NOTE: had in depth investigations for CVE-2018-5727, see upstream bug report
--
openssl (Thorsten Alteholz)
--
pdns (Abhijith PA)
--
qemu (Santiago)
  NOTE: 20181026: no fix yet for recent dsa issues, but start working on
  NOTE: pending no-dsa issues
--
salt (Mike Gabriel)
--
spamassassin (Antoine Beaupre)
--
squid3 (Abhijith PA)
  NOTE:20181101: consider fixing no-dsa issues too. (apo)
  NOTE:20181110: The latest vulnerability is a one line fix. In rest of the no-dsa issues one seems fixed but forgot
  NOTE:20181101: to mention in DLA, and others very intrusive to backport. Substantial change from 3.4 -> 3.5.
--
symfony (Thorsten Alteholz)
  NOTE: 20181110: patches ready, struggling with test suite, waiting for email
--
systemd
  NOTE: 20181101: I recommend to fix all open issues including the postponed
  NOTE: ones, too. (apo)
--
thunderbird (Emilio Pozuelo)
  NOTE: 20181107: package ready, waiting for the stretch update
--
tiff (Brian May)
--
xen
--
